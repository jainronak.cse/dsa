
public class SelectionSort {
	
	public int [] sortArray(int arr[]) {
		int n =arr.length;
		
		for(int i=0;i<n-1;i++) {
			int min=i;
			for(int j=i+1;j<n;j++) {
				if(arr[j]<arr[min]) {
					min=j;
				}
			}
			int temp=arr[min];
			arr[min]=arr[i];
			arr[i]= temp;
		}
		return arr;
	}
	
	public void printArray(int arr[]) {
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
	
	public static void main(String[] args) {
		
		SelectionSort obj = new SelectionSort();
		int arr[] = {20,12,24,3,5,23};
		int brr [] = obj.sortArray(arr);
		obj.printArray(brr);
		
	}
}
