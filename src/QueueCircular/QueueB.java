package QueueCircular;



public class QueueB {

	public static class Queue {
		static int arr[];
		static int size;
		static int front = -1;
		static int rear = -1;

		Queue(int size) {
			this.size = size;
			arr = new int[size];
		}

		public static boolean isEmpty() {
			return rear == -1 && front == -1;
		}

		public static boolean isFull() {
			return (rear + 1) % size == front;
		}

		public void add(int data) {
			if (isFull()) {
				System.out.println("Overflow queue");
				return;
			}

			// if it is the 1st element

			if (front == -1) {
				front = 0;
			}
			rear = (rear + 1) % size;
			arr[rear] = data;
		}

		public static int remove() {
			if (isEmpty()) {
				System.out.println("empty queue");
				return -1;
			}
			int res = arr[front];

			if (front == rear) {
				front = rear = -1;
			} else {
				front = (front + 1) % size;
			}

			return res;
		}
		public static int peek() {
			if (isEmpty()) {
				System.out.println("empty queue");
				return -1;
			}
			return arr[front];
		}
	
	}
	
	public static void main(String[] args) {
		Queue obj = new Queue(5);
		obj.add(1);
		obj.add(2);
		obj.add(3);
		obj.add(4);
		System.out.println(obj.peek());
		System.out.println(obj.remove());
		System.out.println(obj.peek());
		System.out.println(obj.remove());
		System.out.println(obj.peek());
		System.out.println(obj.remove());
		System.out.println(obj.peek());
		System.out.println(obj.remove());
		System.out.println(obj.peek());
		obj.add(5);
		
		System.out.println(obj.peek());
		
	}

}
