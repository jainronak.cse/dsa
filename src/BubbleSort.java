
public class BubbleSort {

	
	public static int[] optimizedSortArray(int[] arr) {
		int n = arr.length;
		boolean flag;
		for (int i = 0; i < n; i++) {
			 flag=false;
			for (int j = 0; j < n - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {

					int temp =arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					flag=true;
				}
			}
			
			if(flag==false) {
				break;
			}
		}
		return arr;

	}
	
	public static int[] sortArray(int[] arr) {
		int n = arr.length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {

					int temp =arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return arr;

	}

	public static void printArr(int arr[]) {
		for(int i=0;i<arr.length;i++ ) {
			System.out.println(arr[i]);
		}
	}
	
	
	public static void main(String[] args) {
		int arr[] = { 5, 1, 4, 2, 8 };
//		int arr1 [] =BubbleSort.sortArray(arr);
		int arr1 [] =BubbleSort.optimizedSortArray(arr);
		BubbleSort.printArr(arr1);
	}
}
