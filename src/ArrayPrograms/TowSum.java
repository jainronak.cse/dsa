package ArrayPrograms;

import java.util.HashMap;
import java.util.Map;

public class TowSum {

	public static  int[] twoSum(int[] arr, int target) {
		int len = arr.length;
		int temp[] = null;
		for (int i = 0; i < len; i++) {
			for (int j = i + 1; j < len; j++) {
				if (arr[i] + arr[j] == target) {
					temp = new int[2];
					temp[0] = i;
					temp[1] = j;
					break;
				}
			}
		}
		return temp;

	}

	
	public static  int[] twoSumUsingMap(int[] nums, int target) {
		 Map<Integer, Integer> numMap = new HashMap<>();
	        for (int i = 0; i < nums.length; i++) {
	            int complement = target - nums[i];
	            if (numMap.containsKey(complement)) {
	                return new int[] { numMap.get(complement), i };
	            } else {
	                numMap.put(nums[i], i);
	            }
	        }
	        return new int[] {};

	}
	
	
	public static void main(String[] args) {
		int arr[]= {1, 3,4,5,6,3};
		int temp[];
		temp=TowSum.twoSumUsingMap(arr,7);
		System.out.println(temp[0] + "   " + temp[1]);
	}
}
