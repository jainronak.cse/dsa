package StringsInJAva;

public class Day1 {

	 public static int countPaths(int i, int j, int m, int n) {
		 System.out.println("("+i+","+j+")");
		 if(i == m-1 || j == n-1) {
	           return 1;
	       }
	       
	       int dPaths = countPaths(i+1, j, m, n);
	       int rPaths = countPaths(i, j+1, m, n);
	 
	       return  dPaths+rPaths ;
	   }
	 
	   public static void main(String args[]) {
	       int m = 3, n = 3;
	       System.out.println(countPaths(0, 0, m, n));
	   }

}
