package StringsInJAva;

public class SumOfDigit {

	public static int sum(String str) {

		char c[] = str.toCharArray();
		int sum = 0;
		for (int i = 0; i < c.length; i++) {
			if (Character.isDigit(c[i])) {
				int a = Integer.parseInt(String.valueOf(c[i]));
				sum = sum + a;
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		int i = SumOfDigit.sum("ro12pp0098poL4");
		System.out.println(i);
	}
}
