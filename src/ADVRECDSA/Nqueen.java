package ADVRECDSA;

import java.util.ArrayList;
import java.util.List;

public class Nqueen {

	public void saveTheBoard(char[][] board, List<List<String>> allBoards) {
		String row = "";
		List<String> newBoard = new ArrayList<>();
		for (int i = 0; i < board.length; i++) {
			row = "";
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 'Q') {
					row += 'Q';
				} else {
					row += '.';
				}

			}
			newBoard.add(row);
		}
		allBoards.add(newBoard);
	}

	public void helper(char[][] board, List<List<String>> allBoards, int col) {
		if (col == board.length) {
			saveTheBoard(board, allBoards);
			return;
		}
		for (int r = 0; r < board.length; r++) {
			if (isSafe(r, col, board)) {
				board[r][col] = 'Q';
				helper(board, allBoards, col + 1);
				board[r][col] = '.';
			}
		}
	}

	public boolean isSafe(int row, int col, char[][] board) {
		// horizontal
		for (int j = 0; j < board.length; j++) {
			if (board[row][j] == 'Q') {
				return false;
			}
		}
		// vertical
		for (int i = 0; i < board.length; i++) {
			if (board[i][col] == 'Q') {
				return false;
			}
		}
		// Upper Left
		int r = row;
		for (int c = col; c >= 0 && r >= 0; c--, r--) {
			if (board[r][c] == 'Q') {
				return false;
			}
		}
		// Upper Right
		r = row;
		for (int c = col; c <board.length && r >= 0; c++, r--) {
			if (board[r][c] == 'Q') {
				return false;
			}
		}

		// Lower Left
		r = row;
		for (int c = col; c >=0 && r<board.length; c--, r++) {
			if (board[r][c] == 'Q') {
				return false;
			}
		}
		// Lower right
		r = row;
		for (int c = col; c <board.length && r<board.length; c++, r++) {
			if (board[r][c] == 'Q') {
				return false;
			}
		}
		return true;

	}

	public List<List<String>> solveNQueens(int n) {
		List<List<String>> allBoards = new ArrayList<>();
		char[][] board = new char[n][n];
		helper(board, allBoards, 0);
		return allBoards;
	}

	public static void main(String[] args) {
		int n = 4;
		Nqueen obj = new Nqueen();
		List<List<String>> allBoards = obj.solveNQueens(n);
		System.out.println(allBoards);
	}
}
