package ADVRECDSA;

public class QuickSort {
	public static int partition(int arr[],int low,int high) {
		int pivot=arr[high];
		int i=(low-1);
		for(int j=low;j<high;j++) {
			if(arr[j]<=pivot) {
				i++;
				int temp= arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
				
			}
		}
//		i++;
		int temp= arr[i+1];
		arr[i+1]=arr[high];
		arr[high]=temp;
		return i+1;
	}

	public static void qSort(int arr[],int low,int high) {
		if(low<high) {
			int piIndx = partition(arr,low,high);
			qSort(arr,low,piIndx-1);
			qSort(arr,piIndx+1,high);
			
		}
	}
	public static void main(String[] args) {
		
		int arr[] = {11,7,8,9,1,5};
		int len= arr.length;
		qSort(arr,0,len-1);
		
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
}
