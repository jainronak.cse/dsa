package StackInJava;

import java.util.ArrayList;
import java.util.Stack;

public class StackClassUsingAL {

	
	class Stack {
//		public Node head;
		ArrayList<Integer> al= new ArrayList<>();

		public boolean isEmpty() {
			return al.size()==0;
		}

		public void push(int data) {
			al.add(data);
		}

		public int pop() {
			if(isEmpty()) {
				return -1;
			}
			int top = al.get(al.size()-1);
			al.remove(al.size()-1);
			return top;
		}
		public int peek() {
			if(isEmpty()) {
				return -1;
			}
			return al.get(al.size()-1);
		}
	}

	
	public static void main(String[] args) {
		StackClass obj  = new StackClass();
		StackClass.Stack s= obj.new Stack();
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		while(!s.isEmpty()) {
			System.out.println(s.peek());
			s.pop();
		}
	}
}
