package StackInJava;

public class NestedClas {

	public void show() {
		System.out.println("in NestedClas");
	}
	static class temp{
		public void show() {
			System.out.println("in temp");
		}
	}
	 class temp1{
		public void show() {
			System.out.println("in temp");
		}
	}
	public static void main(String[] args) {
		NestedClas obj = new NestedClas();
		NestedClas.temp obj1= new NestedClas.temp();
		obj1.show();
		NestedClas.temp1 obj2=  obj.new temp1();
		obj2.show();
	}
}

