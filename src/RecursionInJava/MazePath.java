package RecursionInJava;

public class MazePath {

	public static int countPath(int i,int j,int n,int m) {
       System.out.println("("+i+","+j+")");
		if(i==n-1 || j==m-1) {
			return 1;
		}
		int dPath = countPath(i+1,j,n,m);
		int rPath = countPath(i,j+1,n,m);
		return dPath+rPath;
	}
	public static void main(String[] args) {
		int n=3, m=3;
		
		int tPath = countPath(0,0,n,m);
		System.out.println(tPath);
	}
}
