package RecursionInJava;

public class NoOfCombimnation {

	public static void printPer(String str, String per) {
		if(str.length()==0) {
			System.out.println(per);
			return;
		}
		
		for(int i=0;i<str.length();i++) {
			char currChar= str.charAt(i);
			String str1= str.substring(0,i);
			String str2= str.substring(i+1);
			String newStr= str1+str2;
			printPer(newStr,  per+currChar);
		}
		
		
		
	}
	public static void main(String[] args) {
		String str = "abc";
		
		NoOfCombimnation.printPer(str,  "");	
	}
}
