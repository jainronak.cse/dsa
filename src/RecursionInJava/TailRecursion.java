package RecursionInJava;

public class TailRecursion {
	static int factTR(int n, int a) {
		if (n == 0)
			return a;

		return factTR(n - 1, n * a);
	}

	static int fact1(int n) {
		if (n == 0)
			return 1;

		return n * fact1(n - 1);
	}

	

	// A wrapper over factTR
	static int fact(int n) {
		return factTR(n, 1);
	}

	// Driver code
	static public void main(String[] args) {
		System.out.println(fact1(5));
	}
}
