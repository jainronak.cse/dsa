package RecursionInJava;

public class FactorialUsingJava {

	
	public static int calFactorial(int n) {
		if(n==1 || n==0) {
			return 1;
		}
//		int temp= calFactorial(n-1);
//		int fact = n*temp;
//		return fact;
		return n*calFactorial(n-1);
	}
	
	public static int calFactorialTailRecursive(int n, int a) {
		if( n==0) {
			return a;
		}
		return calFactorialTailRecursive(n-1,n*a);

	}
	public static int temp(int n) {
		return calFactorialTailRecursive(n,1);
	}
	
	
	public static void main(String[] args) {
		int n =FactorialUsingJava.calFactorial(5);
		System.out.println(n);
//		
//		int j =FactorialUsingJava.temp(5);
//		System.out.println(j);
		
	}
}
