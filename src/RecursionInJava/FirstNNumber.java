package RecursionInJava;

public class FirstNNumber {

	
	public int recurSum(int n) {
		if(n<=1) {
			return n;
		}
		return n+recurSum(n-1);
	}
	
	
	public static void main(String[] args) {
		
		FirstNNumber obj = new FirstNNumber();
		int n = obj.recurSum(5);
		System.out.println(n);
	}
	
}
