package RecursionInJava;

public class ReverseString {

	public static void printRev(String str,int index) {
		if(index==0) {
			System.out.println(str.charAt(index));
			return;
		}
		System.out.print(str.charAt(index));
		printRev(str, index-1);
//		System.out.println();
//		System.out.print(str.charAt(index));
		
	}
	
	public static void main(String[] args) {
		String str = "rounak";
		
//		printRev(str, str.length()-1);
		
		String s =str.substring(1,4);
		System.out.println(s);
	}
}
