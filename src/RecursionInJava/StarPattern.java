package RecursionInJava;

public class StarPattern {

	public static void main(String[] args) {
		
//		for(int i=0;i<5;i++) {
//			for(int j=0;j<=i;j++) {
//				System.out.print("*");
//			}
//			System.out.println();
//		}
		StarPattern.printp(5, 1);		
	}
	
	
	public static void printPattern(int n) {
		if(n==0) {
			return;
		}
		System.out.print("*");
		printPattern(n-1);
	}
	public static void printp(int n,int i) {
		if(n==0) {
			return;
		}
		printPattern(i);
		System.out.println();
		printp(n-1,i+1);
	}
}
