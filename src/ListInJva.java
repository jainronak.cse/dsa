import java.util.ArrayList;
import java.util.List;

public class ListInJva {
	
	public static void main(String[] args) {		
		
		List <String> lst = new ArrayList<String>();
		lst.add("rj");
		lst.add("rj1");
		lst.add("rj2");
		System.out.println(lst);
		List <String> lst1 = new ArrayList<String>();
		lst1.add("rj");
		lst1.add("rj1");
		lst1.add("rj2");
		System.out.println(lst1);
		
		List <String> lst4 = new ArrayList<String>();
		lst4.addAll(lst);
		
		List<List <String>> lst3 = new ArrayList<>();
		lst3.add(lst);
		lst3.add(lst1);
		System.out.println(lst3);
		
	}
}
