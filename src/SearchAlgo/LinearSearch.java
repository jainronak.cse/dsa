package SearchAlgo;

public class LinearSearch {

	public static void searchAnElement(int arr[], int elementToSearch) {
		
		int n=arr.length;
		int count=0;
		for(int i=0;i<n;i++) {
			if(arr[i]==elementToSearch) {
				System.out.println("Element is present at index " + i);
			count++;
			}
		}
		
		if(count==0) {
			System.out.println("Element is not present");
		}
	}
	
	
	public static void main(String[] args) {
		
		int arr []= {2,4,3,5,8,1,2};
		
		LinearSearch.searchAnElement(arr, 50);
		
		
		
	}
}
