package LinkedList;

public class LL {

	private int size;
	Node head;

	public LL() {
		size = 0;
	}

	public void reverseIterate() {
		if (head == null || head.next == null) {
			System.out.println("Either list is empty or there is only one element");
		}
		Node prevNode = head;
		Node currentNode = head.next;
		while(currentNode !=null) {
			Node nextNode = currentNode.next;
			currentNode.next = prevNode;
			
			// Update
			prevNode = currentNode;
			currentNode= nextNode;
		}
		head.next=null;
		head=prevNode;

	}

	public class Node {
		String data;
		Node next;

		Node(String data) {
			this.data = data;
			this.next = null;
			size++;
		}
	}

	public int sizeLst() {
		return size;
	}

	public void addFirst(String data) {
		Node newNode = new Node(data);
		if (head == null) {
			head = newNode;
			return;
		}
		newNode.next = head;
		head = newNode;
	}

	public void addLast(String data) {
		Node newNode = new Node(data);
		if (head == null) {
			head = newNode;
			return;
		}

		Node currNode = head;
		while (currNode.next != null) {
			currNode = currNode.next;
		}
		currNode.next = newNode;
	}

	public void printList() {
		if (head == null) {
			System.out.println("Empty list");
			return;
		}
		Node currNode = head;
		while (currNode != null) {
			System.out.print(currNode.data + "-->");
			currNode = currNode.next;
		}
		System.out.println("null");
	}

	public void deleteFirst() {
		if (head == null) {
			System.out.println("Empty list, Nothing to delete");
			return;
		}
		head = this.head.next;
		size--;
	}

	public void deleteLast() {
		if (head == null) {
			System.out.println("Empty list, Nothing to delete");
			return;
		}

		size--;
		if (head.next == null) {
			head = null;
			return;

		}
		Node currNode = head;
		Node lastNode = head.next;
		while (lastNode.next != null) {

			currNode = currNode.next;
			lastNode = lastNode.next;
		}

		currNode.next = null;
	}

	public static void main(String[] args) {
		LL lst = new LL();
		lst.addFirst("list");
		lst.addFirst("Linked");
		
		lst.addFirst("is");
		lst.addFirst("this");
		
		lst.printList();
//		lst.addLast("link");

		lst.reverseIterate();
		lst.printList();
//		System.out.println(lst.sizeLst());
//		lst.deleteFirst();
//		lst.printList();
//		lst.deleteLast();
//		lst.printList();
//		System.out.println(lst.sizeLst());

	}

}
