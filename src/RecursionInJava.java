
public class RecursionInJava {
	
	public static void show(int n) {
		if(n==0) {
			return;
		}
		System.out.println(n);
		show(n-1);
		System.out.println(n);
	}
	
	
	public static void main(String[] args) {
		
		
		RecursionInJava.show(5);
		
		
	}
}
