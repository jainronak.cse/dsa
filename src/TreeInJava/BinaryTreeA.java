package TreeInJava;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTreeA {

	static class Node {
		int data;
		Node left;
		Node right;

		Node(int data) {
			this.data = data;
			this.left = null;
			this.right = null;
		}
	}

	static class BinaryTree {
		static int indx = -1;

		public static Node buildTree(int node[]) {
			indx++;
			if (node[indx] == -1) {
				return null;
			}
			Node newNode = new Node(node[indx]);
			newNode.left = buildTree(node);
			newNode.right = buildTree(node);
			return newNode;

		}
	}

	public static void preOrder(Node root) {
		if (root == null) {
			System.out.print(-1 + " ");
			return;
		}

		System.out.print(root.data + " ");
		preOrder(root.left);
		preOrder(root.right);
	}

	public static void inOrder(Node root) {
		if (root == null) {
//			System.out.print(-1+" ");
			return;
		}

		inOrder(root.left);
		System.out.print(root.data + " ");
		inOrder(root.right);
	}

	public static void postOrder(Node root) {
		if (root == null) {
//			System.out.print(-1+" ");
			return;
		}

		postOrder(root.left);
		
		postOrder(root.right);
		System.out.print(root.data + " ");
		
	}
	
	
	public static void levelOrder(Node root) {
		if(root==null) {
			return;
		}
		Queue <Node> q= new LinkedList<>();
		String s;
		q.add(root);
		q.add(null);
		while(!q.isEmpty()) {
			Node curr= q.remove();
			if(curr==null) {
				System.out.println();
				if(q.isEmpty()) {
					break;
				}else {
					q.add(null);
				}
			}else {
				System.out.print(curr.data+" ");
				if(curr.left!=null) {
					q.add(curr.left);
				}
				if(curr.right!=null) {
					q.add(curr.right);
				}
			}
		}
	}
	
	
	
	
	
	public static void main(String[] args) {
		int node[] = { 1, 2, 4, -1, -1, 5, -1, -1, 3, -1, 6, -1, -1 };
		BinaryTree obj = new BinaryTree();
		Node root = obj.buildTree(node);
//		System.out.println(root.data);
//		System.out.println(root.left.data);
//		System.out.println(root.right.data);

//		preOrder(root);
//		System.out.println();
//		inOrder(root);
//		System.out.println();
//		postOrder(root);
		levelOrder(root);
	}

}
